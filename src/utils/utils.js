export const hideLongString = (desciption, length) => {
  let lengthDescription = desciption.length;

  if (lengthDescription > length) {
    return desciption.slice(0, length) + "...";
  } else {
    return desciption;
  }
};

export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjIwLzAxLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NDE3MjgwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc0MzIwNDAwfQ.8_aCoaa6rU0qnQpITJH8MZSFEBfvbj11eFJWuFsTYL8";

export let colors = ["#8ecae6", "#219ebc", "#023047", "#ffb703", "#fb8500"];
