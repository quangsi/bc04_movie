import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";
import HomePage from "./Pages/HomePage/HomePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import Header from "./Components/Header/Header";
import DemoHook from "./Pages/DemoHook/DemoHook";
import Ex_colors from "./Pages/Ex_colors/Ex_colors";

function App() {
  return (
    <div className=" container">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/ex-color" component={Ex_colors} />
          {/* <Route
            path="/detail/:id"
            render={() => {
              return <DetailPage data="124" />;
            }}
          /> */}
          <Route path="/detail/:id" component={DetailPage} />
          <Route path="/demo-hook" component={DemoHook} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
