import axios from "axios";
import React, { Component } from "react";
import { TOKEN_CYBERSOFT } from "../../utils/utils";

export default class DetailPage extends Component {
  state = {
    detail: null,
  };
  componentDidMount() {
    let { id } = this.props.match.params;

    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ detail: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    let { detail } = this.state;
    // optional chaining
    return (
      <div className="container">
        <p className="display-4">{detail?.tenPhim}</p>
        <img src={detail?.hinhAnh} style={{ height: 500 }} alt="" />
        <p className="">{detail?.moTa}</p>
      </div>
    );
  }
}
