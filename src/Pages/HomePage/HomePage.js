import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Header from "../../Components/Header/Header";
import { hideLongString, TOKEN_CYBERSOFT } from "../../utils/utils";

export default class HomePage extends Component {
  state = {
    movieList: [],
  };
  componentDidMount() {
    // check user đã đăng nhập hay chưa

    let userJson = localStorage.getItem("USER");

    if (!JSON.parse(userJson)) {
      // chuyển hướng về trang login nếu user chưa đăng nhập
      window.location.href = "/login";
    } else {
    }

    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ movieList: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderContent = () => {
    return this.state.movieList.map((movie) => {
      console.log("movie: ", movie);
      return (
        <div className="card col-3" style={{ width: "18rem" }}>
          <img
            className="card-img-top"
            src={movie.hinhAnh}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{movie.tenPhim}</h5>
            <p className="card-text">{hideLongString(movie.moTa, 30)}</p>
            <NavLink to={`/detail/${movie.maPhim}`} className="btn btn-primary">
              Xem chi tiết
            </NavLink>
          </div>
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
