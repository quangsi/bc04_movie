import React, { useState } from "react";
import { colors } from "../../utils/utils";

export default function Ex_colors() {
  const [color, setColor] = useState(colors[0]);

  const handleSetColor = (value) => {
    setColor(value);
  };
  let renderButtonColor = () => {
    return colors.map((color) => {
      return (
        <button
          style={{ backgroundColor: color, width: 200 }}
          className="btn d-block text-white my-5"
          onClick={() => {
            handleSetColor(color);
          }}
        >
          {color}
        </button>
      );
    });
  };
  return (
    <div className="row">
      <div
        style={{ backgroundColor: color, aspectRatio: "1/1" }}
        className="col-6"
      ></div>

      <div className="col-6">{renderButtonColor()}</div>
    </div>
  );
}
